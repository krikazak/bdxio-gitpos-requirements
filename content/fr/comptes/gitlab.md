---
title: "1/2 - Créer un compte Gitlab"
date: 1042-04-01T00:00:00+01:00
tags: []
featured_image: ""
description: ""
---
__*(si vous n’en avez pas)*__


Connectez vous sur https://gitlab.com/users/sign_up


Vous pouvez au choix :  
- soit créer un compte en remplissant les différents champs 
- soit vous enregistrer via votre un de vos comptes suivant : google | github | twitter | bitbucket | Salesforce (lien bas de l’écran)



![Creation d'un compte gitlab](../images/01-create-gitlab-account.png)

Une fois, votre compte Gitlab créé et validé (via le mail de validation), connectez-vous à Gitlab, répondez aux différentes questions. 

Une fois que le compte Gitlab est fonctionnel, vous pouvez [créer votre compte Gitpod](../gitpod/)
